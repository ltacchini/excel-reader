import java.io.IOException;
import java.net.URL;

import org.apache.poi.EncryptedDocumentException;

import config.DefaultConfig;
import inputLoader.ExcelInputLoaderFactory;
import lectura.InputLoader;

public class MainJava {

    public static void main(String[] args) throws EncryptedDocumentException, IOException{
    	DefaultConfig fileConfig = new DefaultConfig("config.properties");
		String path = fileConfig.getElement("path");	
		String sheetName = fileConfig.getElement("sheet");
		
		URL url = new URL("file:"+path+"#"+sheetName); 
		System.out.println(url);
		
		ExcelInputLoaderFactory inputLoaderFactory = new ExcelInputLoaderFactory();
		InputLoader inputLoader = inputLoaderFactory.createInputLoader(url);
    	Object [] ret = inputLoader.next();
    	
    	for (int i = 0; i < ret.length; i++) {
    		System.out.print(ret[i] + " ");
    	}
	    	
    	Object [] ret1 = inputLoader.next();
    	System.out.print("\n");
    	for (int i = 0; i < ret1.length; i++) {
    		System.out.print(ret1[i] + ",");
    	}
    	
    	Object [] ret2 = inputLoader.next();
    	System.out.print("\n");
    	for (int i = 0; i < ret2.length; i++) {
    		System.out.print(ret2[i] + ",");
    	}
    	Object [] ret3 = inputLoader.next();
    	System.out.print("\n");
    	for (int i = 0; i < ret3.length; i++) {
    		System.out.print(ret3[i] + ",");
    	}
    	System.out.print("\n");
    }
    
}