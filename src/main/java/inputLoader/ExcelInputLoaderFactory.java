package inputLoader;

import java.net.URL;

import lectura.InputLoader;
import lectura.InputLoaderFactory;

public class ExcelInputLoaderFactory implements InputLoaderFactory {
	
	ExcelInputLoader inputLoader;

	@Override
	public InputLoader createInputLoader(URL url) {
		inputLoader = new ExcelInputLoader(url); 
		return inputLoader;
	}

	@Override
	public boolean acceptsURL(URL url) { 
		return url.getPath().endsWith(".xls") || url.getPath().endsWith(".xlsx"); 
	}
	
}
